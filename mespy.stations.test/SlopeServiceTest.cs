﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Microsoft.EntityFrameworkCore;
using mespy.stations.DAL;
using mespy.stations.Services;

namespace mespy.stations.test
{
    [TestClass]
    public class SlopeServiceTest
    {
        [TestMethod]
        public void TestAddSlope()
        {
            var mockSet = new Mock<DbSet<Slope>>();
            var mockContext = new Mock<MespyInfosContext>();
            mockContext.Setup(m => m.Slopes).Returns(mockSet.Object);

            var service = new SlopeService(mockContext.Object);

            Slope slope = new Slope()
            {
                Id = 1,
                StationId = 1,
                Name = "Test",
                LevelId = 1,
                Length = 1,
                MinAltitude = 1,
                MaxAltitude = 1
            };

            service.AddSlope(slope);

            mockSet.Verify(m => m.Add(It.IsAny<Slope>()), Times.AtLeast(1));
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void GetAllSlopesByStationSimple()
        {
            var slopesListData = GetFakeSlopesList();
            var mockContext = GetFakeContext(slopesListData);

            var service = new SlopeService(mockContext.Object);
            var slopes = service.GetAllSlopesByStation(1);

            Assert.AreEqual(1, slopes.Count);
            Assert.AreEqual(slopes[0].Length, 100);
        }

        [TestMethod]
        public void GetAllSlopesByStationMultiResult()
        {
            var slopesListData = GetFakeSlopesList();
            var mockContext = GetFakeContext(slopesListData);

            var service = new SlopeService(mockContext.Object);
            var slopes = service.GetAllSlopesByStation(3);

            Assert.IsTrue(slopes.Count > 1);
            Assert.AreEqual(slopes[1].Length, 400);
        }

        private IQueryable<Slope> GetFakeSlopesList()
        {
            var data = new List<Slope>
            {
                new Slope { Id = 1, StationId = 1, Name = "Test1", LevelId = 1, Length = 100, MinAltitude = 1000, MaxAltitude = 1500 },
                new Slope { Id = 2, StationId = 2, Name = "Test2", LevelId = 2, Length = 200, MinAltitude = 1000, MaxAltitude = 2000 },
                new Slope { Id = 3, StationId = 3, Name = "Test3", LevelId = 3, Length = 300, MinAltitude = 1000, MaxAltitude = 2500 },
                new Slope { Id = 4, StationId = 3, Name = "Test4", LevelId = 3, Length = 400, MinAltitude = 1500, MaxAltitude = 3000 },
            };

            return data.AsQueryable();
        }

        private Mock<MespyInfosContext> GetFakeContext(IQueryable<Slope> fakeData)
        {
            var mockSet = new Mock<DbSet<Slope>>();
            mockSet.As<IQueryable<Slope>>().Setup(m => m.Provider).Returns(fakeData.Provider);
            mockSet.As<IQueryable<Slope>>().Setup(m => m.Expression).Returns(fakeData.Expression);
            mockSet.As<IQueryable<Slope>>().Setup(m => m.ElementType).Returns(fakeData.ElementType);
            mockSet.As<IQueryable<Slope>>().Setup(m => m.GetEnumerator()).Returns(fakeData.GetEnumerator());

            var mockContext = new Mock<MespyInfosContext>();
            mockContext.Setup(c => c.Slopes).Returns(mockSet.Object);

            return mockContext;
        }
    }
}
