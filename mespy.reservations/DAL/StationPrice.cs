﻿using System;
using System.Collections.Generic;

namespace mespy.reservations.DAL
{
    public partial class StationPrice
    {
        public StationPrice()
        {
            StationReservations = new HashSet<StationReservation>();
        }

        public int Id { get; set; }
        public int StationId { get; set; }
        public string Name { get; set; } = null!;
        public float Value { get; set; }
        public string? Description { get; set; }

        public virtual ICollection<StationReservation> StationReservations { get; set; }
    }
}
