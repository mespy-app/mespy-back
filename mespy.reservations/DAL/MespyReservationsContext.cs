﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace mespy.reservations.DAL
{
    public partial class MespyReservationsContext : DbContext
    {
        public MespyReservationsContext()
        {
        }

        public MespyReservationsContext(DbContextOptions<MespyReservationsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<StationPrice> StationPrices { get; set; } = null!;
        public virtual DbSet<StationReservation> StationReservations { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=127.0.0.1;port=3306;user=root;password=root;database=MespyReservations", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.22-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<StationPrice>(entity =>
            {
                entity.ToTable("StationPrice");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<StationReservation>(entity =>
            {
                entity.ToTable("StationReservation");

                entity.HasIndex(e => e.StationPriceId, "StationPriceId");

                entity.Property(e => e.EmailReservation).HasMaxLength(60);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ReservationDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.StationPrice)
                    .WithMany(p => p.StationReservations)
                    .HasForeignKey(d => d.StationPriceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("StationReservation_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
