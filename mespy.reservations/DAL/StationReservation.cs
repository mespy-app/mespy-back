﻿using System;
using System.Collections.Generic;

namespace mespy.reservations.DAL
{
    public partial class StationReservation
    {
        public int Id { get; set; }
        public int StationPriceId { get; set; }
        public sbyte Nbr { get; set; }
        public string EmailReservation { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime ReservationDate { get; set; }

        public virtual StationPrice StationPrice { get; set; } = null!;
    }
}
