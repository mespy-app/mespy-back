﻿
using AutoMapper;
using mespy.reservations.DAL;
using mespy.reservations.Models;
using mespy.reservations.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace mespy.reservations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StationReservationController : ControllerBase
    {
        private readonly IStationReservationService stationReservationService;
        private readonly ILogger<StationReservationController> logger;
        private readonly IMapper mapper;

        public StationReservationController(IStationReservationService stationReservationService, ILogger<StationReservationController> logger, IMapper mapper)
        {
            this.stationReservationService = stationReservationService;
            this.logger = logger;
            this.mapper = mapper;
        }

        [HttpGet("{stationId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetAllStationReservationByStationId([FromRoute] int stationId)
        {
            try
            {
                logger.LogInformation("Start get Station reservations for station : {0}", stationId);

                var stationReservation = stationReservationService.GetStationReservationsByStationId(stationId);

                if (stationReservation != null)
                {
                    logger.LogDebug("Station {0} found", stationId);
                    var stationReservationDTO = mapper.Map<List<StationReservationDTO>>(stationReservation);
                    logger.LogInformation("{0} Reservations found for station: {1}", stationReservation.Count, stationId);
                    return Ok(stationReservationDTO);
                }
                else
                {
                    logger.LogInformation("Station {0} not found", stationId);
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult AddStationReservation([FromBody] StationReservationDTO stationReservationDTO)
        {
            try
            {
                logger.LogInformation("Start add a new station reservation");

                if (stationReservationDTO != null)
                {
                    logger.LogInformation("Add a new station reservation for price : {0}", stationReservationDTO.StationPriceId);
                    var reservation = mapper.Map<StationReservation>(stationReservationDTO);
                    stationReservationService.AddStationReservation(reservation);
                    logger.LogInformation("Reservation added");
                    return Ok(stationReservationDTO);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }
        }
    }
}
