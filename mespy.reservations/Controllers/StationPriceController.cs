﻿using AutoMapper;
using mespy.reservations.DAL;
using mespy.reservations.Models;
using mespy.reservations.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace mespy.reservations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StationPriceController : ControllerBase
    {

        private readonly IStationPriceService stationPriceService;
        private readonly ILogger<StationPriceController> logger;
        private readonly IMapper mapper;

        public StationPriceController(IStationPriceService stationPriceService, ILogger<StationPriceController> logger, IMapper mapper)
        {
            this.stationPriceService = stationPriceService;
            this.logger = logger;
            this.mapper = mapper;
        }

        [HttpGet("{stationId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetAllPriceByStationId([FromRoute] int stationId)
        {
            try
            {
                logger.LogInformation("Start get Station price for station : {0}", stationId);

                var stationPrice = stationPriceService.GetStationPriceByStationId(stationId);

                if (stationPrice != null)
                {
                    logger.LogDebug("Station {0} found", stationId);
                    var stationPriceDTO = mapper.Map<List<StationPriceDTO>>(stationPrice);
                    logger.LogInformation("{0} Prices found for station: {1}", stationPrice.Count, stationId);
                    return Ok(stationPriceDTO);
                }
                else
                {
                    logger.LogInformation("Station {0} not found", stationId);
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult AddStationPrice([FromBody] StationPriceDTO stationPriceDTO)
        {
            try
            {
                logger.LogInformation("Start add a new station price");

                if (stationPriceDTO != null)
                {
                    logger.LogInformation("Add new station price to station : {0}", stationPriceDTO.StationId);
                    var stationPrice = mapper.Map<StationPrice>(stationPriceDTO);
                    stationPriceService.AddStationPrice(stationPrice);
                    logger.LogInformation("Price to station {0} added", stationPrice.StationId);
                    return Ok(stationPrice);
                } 
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }
        }
    }
}
