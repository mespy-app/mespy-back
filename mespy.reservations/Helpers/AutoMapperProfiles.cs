﻿using AutoMapper;
using mespy.reservations.DAL;
using mespy.reservations.Models;

namespace mespy.reservations.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<StationPrice, StationPriceDTO>();
            CreateMap<StationPriceDTO, StationPrice>();
            CreateMap<StationReservationDTO, StationReservation>();
            CreateMap<StationReservation, StationReservationDTO>();
        }
    }
}
