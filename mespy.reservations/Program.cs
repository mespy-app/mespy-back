using mespy.reservations.DAL;
using mespy.reservations.Helpers;
using mespy.reservations.Services;
using mespy.reservations.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);

// Configure custom services
builder.Services.AddScoped<IStationPriceService, StationPriceService>();
builder.Services.AddScoped<IStationReservationService, StationReservationService>();

// Configure DB connection
string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
var dbVersion = new MariaDbServerVersion(new Version(8, 0, 22));
builder.Services.AddDbContext<MespyReservationsContext>(dbContextOption => dbContextOption.UseMySql(connectionString, dbVersion));


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
