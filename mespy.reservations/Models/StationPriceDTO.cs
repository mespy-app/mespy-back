﻿namespace mespy.reservations.Models
{
    public class StationPriceDTO
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public string Name { get; set; } = null!;
        public float Value { get; set; }
        public string? Description { get; set; }

    }
}
