﻿namespace mespy.reservations.Models
{
    public class StationReservationDTO
    {
        public int Id { get; set; }
        public int StationPriceId { get; set; }
        public sbyte Nbr { get; set; }
        public string EmailReservation { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime ReservationDate { get; set; }

    }
}
