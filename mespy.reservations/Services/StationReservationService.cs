﻿using mespy.reservations.DAL;
using mespy.reservations.Services.Interfaces;

namespace mespy.reservations.Services
{
    public class StationReservationService : IStationReservationService
    {
        private readonly MespyReservationsContext _context;

        public StationReservationService(MespyReservationsContext context)
        {
            _context = context;
        }

        public void AddStationReservation(StationReservation stationReservation)
        {
            _context.Add(stationReservation);
            _context.SaveChanges();
        }

        public List<StationReservation> GetStationReservationsByStationId(int stationId)
        {
            return _context.StationPrices.Join(_context.StationReservations, price => price.Id, reservation => reservation.StationPrice.Id, (price, reservation) => reservation).ToList(); 
        }
    }
}
