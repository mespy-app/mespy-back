﻿
using mespy.reservations.DAL;
using mespy.reservations.Services.Interfaces;

namespace mespy.reservations.Services
{
    public class StationPriceService : IStationPriceService
    {
        private readonly MespyReservationsContext _context;
        public StationPriceService(MespyReservationsContext context)
        {
            _context = context;
        }
        public void AddStationPrice(StationPrice stationPrice)
        {
            _context.StationPrices.Add(stationPrice);
            _context.SaveChanges();
        }

        public List<StationPrice> GetStationPriceByStationId(int stationId)
        {
            return _context.StationPrices.Where(w => w.StationId == stationId).ToList();
        }
    }
}
