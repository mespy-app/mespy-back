﻿

using mespy.reservations.DAL;

namespace mespy.reservations.Services.Interfaces
{
    public interface IStationPriceService
    {
        public List<StationPrice> GetStationPriceByStationId(int stationId);
        public void AddStationPrice(StationPrice stationPrice);
    }
}
