﻿using mespy.reservations.DAL;

namespace mespy.reservations.Services.Interfaces
{
    public interface IStationReservationService
    {
        public void AddStationReservation(StationReservation stationReservation);
        public List<StationReservation> GetStationReservationsByStationId(int stationId);
    }
}
