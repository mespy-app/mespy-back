﻿using mespy.reservations.DAL;
using mespy.reservations.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mespy.reservations.test
{
    [TestClass]
    public class StationPriceTest
    {
        private StationPriceService _service;

        [TestInitialize]
        public void Startup()
        {
            var stationPriceListData = GetFakeStationPriceList();
            var mockContext = GetFakeContext(stationPriceListData);
            this._service = new StationPriceService(mockContext.Object);
        }

        #region GET tests

        [TestMethod]
        public void GetAllStationPriceSimple()
        {
            var stationPrice = _service.GetStationPriceByStationId(1);

            Assert.AreEqual(1, stationPrice.Count);
            Assert.AreEqual(stationPrice[0].Value, 10);
        }

        [TestMethod]
        public void GetAllStationPriceMultiplePrice()
        {
            var stationPrice = _service.GetStationPriceByStationId(3);

            Assert.AreEqual(2, stationPrice.Count);
            Assert.AreEqual(stationPrice[0].Value, 30);
        }

        [TestMethod]
        public void GetAllStationPriceNullParam()
        {
            var stationPrice = _service.GetStationPriceByStationId(0);

            Assert.AreEqual(0, stationPrice.Count);
        }

        #endregion

        #region Insert test

        [TestMethod]
        public void AddStationPriceOptimal()
        {
            var mockSet = new Mock<DbSet<StationPrice>>();
            var mockContext = new Mock<MespyReservationsContext>();
            mockContext.Setup(m => m.StationPrices).Returns(mockSet.Object);

            var service = new StationPriceService(mockContext.Object);

            StationPrice price = new StationPrice()
            {
                Id = 1,
                StationId = 1,
                Name = "Test",
                Value = 10,
                Description = "Unit Test"
            };

            service.AddStationPrice(price);

            mockSet.Verify(m => m.Add(It.IsAny<StationPrice>()), Times.AtLeast(1));
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        #endregion

        private IQueryable<StationPrice> GetFakeStationPriceList()
        {
            var data = new List<StationPrice>
            {
                new StationPrice { Id = 1, StationId = 1, Name = "Test1", Value = 10, Description = "Desc 1" },
                new StationPrice { Id = 2, StationId = 2, Name = "Test2", Value = 20, Description = "Desc 2" },
                new StationPrice { Id = 3, StationId = 3, Name = "Test3", Value = 30, Description = "Desc 3" },
                new StationPrice { Id = 4, StationId = 3, Name = "Test4", Value = 400, Description = "Desc 4" },
            };

            return data.AsQueryable();
        }

        private Mock<MespyReservationsContext> GetFakeContext(IQueryable<StationPrice> fakeData)
        {
            var mockSet = new Mock<DbSet<StationPrice>>();
            mockSet.As<IQueryable<StationPrice>>().Setup(m => m.Provider).Returns(fakeData.Provider);
            mockSet.As<IQueryable<StationPrice>>().Setup(m => m.Expression).Returns(fakeData.Expression);
            mockSet.As<IQueryable<StationPrice>>().Setup(m => m.ElementType).Returns(fakeData.ElementType);
            mockSet.As<IQueryable<StationPrice>>().Setup(m => m.GetEnumerator()).Returns(fakeData.GetEnumerator());

            var mockContext = new Mock<MespyReservationsContext>();
            mockContext.Setup(c => c.StationPrices).Returns(mockSet.Object);

            return mockContext;
        }
    }

}
