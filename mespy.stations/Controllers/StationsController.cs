﻿using AutoMapper;
using mespy.stations.DAL;
using mespy.stations.Models;
using mespy.stations.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace mespy.stations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StationsController : ControllerBase
    {

        private readonly IStationService stationService;
        private readonly IMapper mapper;
        private readonly ILogger<StationsController> logger;
        public StationsController(IStationService stationService, IMapper mapper, ILogger<StationsController> logger)
        {
            this.mapper = mapper;
            this.stationService = stationService;
            this.logger = logger;
        }

        /// <summary>
        /// Get all the stations available.
        /// </summary>
        /// <returns>List of stations</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetStations()
        {
            try
            {
                logger.LogInformation("Start get all stations");
                var stations = stationService.GetAllStations();
                var stationsDTO = mapper.Map<List<StationDTO>>(stations);
                logger.LogInformation("{0} stations found", stationsDTO.Count);
                return Ok(stationsDTO);
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Get a specific station by an id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetStation([FromRoute] int id)
        {
            try
            {
                logger.LogInformation("Start get the station: {0}", id);
                Station? station = stationService.GetStation(id);

                if (station != null)
                {
                    var stationDto = mapper.Map<StationDTO>(station);
                    logger.LogInformation("Station {0} found", id);
                    return Ok(stationDto);
                }
                else
                {
                    logger.LogInformation("Station {0} not found", id);
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }

        }
    }
}
