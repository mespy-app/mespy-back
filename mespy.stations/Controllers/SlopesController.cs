﻿using AutoMapper;
using mespy.stations.DAL;
using mespy.stations.Models;
using mespy.stations.Services;
using mespy.stations.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace mespy.stations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SlopesController : ControllerBase
    {

        private readonly IMapper mapper;
        private readonly ILogger<SlopesController> logger;
        private readonly ISlopeService slopeService;
        private readonly IStationService stationService;

        public SlopesController(ISlopeService slopeService, IStationService stationService, IMapper mapper, ILogger<SlopesController> logger)
        {
            this.mapper = mapper;
            this.logger = logger;
            this.slopeService = slopeService;
            this.stationService = stationService;
        }

        /// <summary>
        /// Public endpoint to get all slopes of a station.
        /// </summary>
        /// <param name="stationId">Station identifier</param>
        /// <returns></returns>
        [HttpGet("{stationId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetAllSlopesByStation([FromRoute] int stationId)
        {
            try
            {
                logger.LogInformation("Start get slopes for station : {0}", stationId);

                var station = stationService.GetStation(stationId);

                if(station != null)
                {
                    logger.LogDebug("Station {0} found", stationId);
                    var slopes = slopeService.GetAllSlopesByStation(stationId);
                    var slopesDTO = mapper.Map<List<SlopeDTO>>(slopes);
                    logger.LogInformation("{0} Slopes found for station: {1}", slopes.Count, stationId);
                    return Ok(slopesDTO);
                } else
                {
                    logger.LogInformation("Station {0} not found", stationId);
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Public endpoint to add a new slope.
        /// </summary>
        /// <param name="slopeDTO">Slope properties to insert</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult AddSlope([FromBody] SlopeDTO slopeDTO)
        {
            try
            {
                logger.LogInformation("Start add new slope");
                if (slopeDTO != null)
                {
                    logger.LogInformation("Add new slope to station : {0}", slopeDTO.StationId);
                    var slope = mapper.Map<Slope>(slopeDTO);
                    slopeService.AddSlope(slope);
                    logger.LogInformation("Slope to station {0} added", slopeDTO.StationId);
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
                
            }
            catch (Exception ex)
            {
                logger.LogError("A new error appear : {0}", ex.Message);
                return StatusCode(500);
            }
        }
    }
}
