﻿using System;
using System.Collections.Generic;

namespace mespy.stations.DAL
{
    public partial class Slope
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public string Name { get; set; } = null!;
        public int LevelId { get; set; }
        public int Length { get; set; }
        public int MinAltitude { get; set; }
        public int MaxAltitude { get; set; }

        public virtual Level Level { get; set; } = null!;
        public virtual Station Station { get; set; } = null!;
    }
}
