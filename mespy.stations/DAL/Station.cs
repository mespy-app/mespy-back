﻿using System;
using System.Collections.Generic;

namespace mespy.stations.DAL
{
    public partial class Station
    {
        public Station()
        {
            Slopes = new HashSet<Slope>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<Slope> Slopes { get; set; }
    }
}
