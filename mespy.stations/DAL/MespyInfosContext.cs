﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace mespy.stations.DAL
{
    public partial class MespyInfosContext : DbContext
    {
        public MespyInfosContext()
        {
        }

        public MespyInfosContext(DbContextOptions<MespyInfosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Level> Levels { get; set; } = null!;
        public virtual DbSet<Slope> Slopes { get; set; } = null!;
        public virtual DbSet<Station> Stations { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=127.0.0.1;port=3306;user=root;password=root;database=MespyInfos", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.22-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<Level>(entity =>
            {
                entity.ToTable("Level");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(20);
            });

            modelBuilder.Entity<Slope>(entity =>
            {
                entity.ToTable("Slope");

                entity.HasIndex(e => e.LevelId, "LevelId");

                entity.HasIndex(e => e.StationId, "StationId");

                entity.Property(e => e.Name).HasMaxLength(30);

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.Slopes)
                    .HasForeignKey(d => d.LevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Slope_ibfk_2");

                entity.HasOne(d => d.Station)
                    .WithMany(p => p.Slopes)
                    .HasForeignKey(d => d.StationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Slope_ibfk_1");
            });

            modelBuilder.Entity<Station>(entity =>
            {
                entity.ToTable("Station");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
