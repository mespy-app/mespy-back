﻿
using AutoMapper;
using mespy.stations.DAL;
using mespy.stations.Models;

namespace mespy.stations.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Station, StationDTO>();
            CreateMap<Slope, SlopeDTO>();
            CreateMap<SlopeDTO, Slope>();
        }
    }
}
