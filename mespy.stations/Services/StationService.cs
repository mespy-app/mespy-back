﻿using mespy.stations.DAL;

namespace mespy.stations.Services
{
    public class StationService : IStationService
    {

        private readonly MespyInfosContext _context;

        public StationService (MespyInfosContext context)
        {
            _context = context;
        }

        public List<Station> GetAllStations()
        {
            return _context.Stations.ToList();
        }

        public Station? GetStation(int id)
        {
            return _context.Stations.Where(w => w.Id == id).FirstOrDefault();
        }
    }
}
