﻿using mespy.stations.DAL;
using mespy.stations.Services.Interfaces;

namespace mespy.stations.Services
{
    public class SlopeService : ISlopeService
    {
        private readonly MespyInfosContext _context;

        public SlopeService(MespyInfosContext context)
        {
            _context = context;
        }

        public List<Slope> GetAllSlopesByStation(int stationId)
        {
            return _context.Slopes.Where(w => w.StationId == stationId).ToList();
        }

        public void AddSlope(Slope slope)
        {
            _context.Slopes.Add(slope);
            _context.SaveChanges();
        }
    }
}
