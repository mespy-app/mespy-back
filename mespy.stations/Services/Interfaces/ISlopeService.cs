﻿using mespy.stations.DAL;

namespace mespy.stations.Services.Interfaces
{
    public interface ISlopeService
    {
        /// <summary>
        /// Get the list of slopes by station.
        /// </summary>
        /// <param name="stationId">Identifier of the station</param>
        /// <returns></returns>
        public List<Slope> GetAllSlopesByStation(int stationId);
        /// <summary>
        /// Add a new slope.
        /// </summary>
        /// <param name="slope"></param>
        public void AddSlope(Slope slope);
    }
}
