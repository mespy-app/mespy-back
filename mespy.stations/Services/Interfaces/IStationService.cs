﻿using mespy.stations.DAL;

namespace mespy.stations.Services
{
    public interface IStationService
    {
        /// <summary>
        /// Get the list of all stations.
        /// </summary>
        /// <returns></returns>
        public List<Station> GetAllStations();

        /// <summary>
        /// Get a station by his identifier.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Station? GetStation(int id);
    }
}
