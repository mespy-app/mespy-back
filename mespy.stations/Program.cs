using mespy.stations.DAL;
using mespy.stations.Helpers;
using mespy.stations.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using mespy.stations.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Configure general services
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);

builder.Logging.ClearProviders();
builder.Logging.AddConsole();


// Configure custom services
builder.Services.AddScoped<IStationService, StationService>();
builder.Services.AddScoped<ISlopeService, SlopeService>();

// Configure DB connection
string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
var dbVersion = new MariaDbServerVersion(new Version(8, 0, 22));
builder.Services.AddDbContext<MespyInfosContext>(dbContextOption => dbContextOption.UseMySql(connectionString, dbVersion));


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
