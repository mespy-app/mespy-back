﻿namespace mespy.stations.Models
{
    public class StationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
    }
}
