﻿namespace mespy.stations.Models
{
    public class SlopeDTO
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public string Name { get; set; } = null!;
        public int LevelId  { get; set; }
        public int Length { get; set; }
        public int MinAltitude { get; set; }
        public int MaxAltitude { get; set; }
    }
}
